/**
 * @(#) LicencePlongeur.java
 */
package FFSSM;

import java.time.LocalDate;
import java.time.temporal.TemporalUnit;

public class Licence {

    private Personne possesseur;

    private String numero;

    private LocalDate delivrance;

    private Club club;

    public Licence(Personne possesseur, String numero, LocalDate delivrance, Club club) {
        this.possesseur = possesseur;
        this.numero = numero;
        this.delivrance = delivrance;
        this.club = club;
    }

    public Personne getPossesseur() {
        return possesseur;
    }

    public String getNumero() {
        return numero;
    }

    public LocalDate getDelivrance() {
        return delivrance;
    }

    public Club getClub() {
        return club;
    }

    /**
     * Est-ce que la licence est valide à la date indiquée ?
     * @param d la date à tester
     * @return vrai si valide à la date d
     **/
    public boolean estValide(LocalDate d) {
        // On calcule la date de non-validité (date de délivrance + 1 an et 1 jour)
        LocalDate limiteDeValidite = delivrance.plusYears(1).plusDays(1);
        LocalDate unJourAvant = delivrance.minusDays(1);
        
        return d.isAfter(unJourAvant) && d.isBefore(limiteDeValidite);
    }

}
