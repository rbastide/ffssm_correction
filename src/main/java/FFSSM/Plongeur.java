package FFSSM;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Plongeur extends Personne {
	private final List<Licence> licences = new LinkedList<>();
	
	private int niveau = 0;
	
	private GroupeSanguin groupe = GroupeSanguin.APLUS;	
	
	public Plongeur(String numeroINSEE, String nom, String prenom, String adresse, String telephone, LocalDate naissance) {
		super(numeroINSEE, nom, prenom, adresse, telephone, naissance);
	}	
	
        public void ajouteLicence(String numero, LocalDate delivrance, Club c) {
            Licence nouvelle = new Licence(this, numero, delivrance, c);
            licences.add(nouvelle);
        }
        
        public void ajouteLicence(Licence nouvelle) {
            if (nouvelle == null)
                throw new IllegalArgumentException("La licence doit être renseignée");
            licences.add(nouvelle);
        }
       
	
	public Licence derniereLicence() {
		// Il y en a forcément une
		return licences.get(licences.size() - 1);
	}

	/**
	 * Get the value of groupe
	 *
	 * @return the value of groupe
	 */
	public GroupeSanguin getGroupe() {
		return groupe;
	}

	/**
	 * Set the value of groupe
	 *
	 * @param groupe new value of groupe
	 */
	public void setGroupe(GroupeSanguin groupe) {
		this.groupe = groupe;
	}


	/**
	 * Get the value of niveau
	 *
	 * @return the value of niveau
	 */
	public int getNiveau() {
		return niveau;
	}

	/**
	 * Set the value of niveau
	 *
	 * @param niveau new value of niveau
	 */
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	
}
