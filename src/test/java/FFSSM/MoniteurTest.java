package FFSSM;

import java.time.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;




public class MoniteurTest {
	
	Club biarritz, banyuls;
	Moniteur rambo;

	@BeforeEach
	public void setUp() {
		biarritz = new Club(null, "Biarritz", null);
		banyuls  = new Club(null, "Banyuls-sur-mer", null);
		rambo = new Moniteur("1 61 03 34", "Rambo", "John", null, null, null, 12345);				
	}
	
	@Test
	public void testEmbauche() {
		assertFalse(rambo.employeurActuel().isPresent());
		rambo.nouvelleEmbauche(banyuls, LocalDate.of(2015, 1, 1)); // 01/01/2015;);
		rambo.nouvelleEmbauche(biarritz, LocalDate.of(2015, 4, 22)); // 22/04/2015;);
		assertSame(biarritz, rambo.employeurActuel().get());
		assertEquals(2, rambo.emplois().size());
	}
}
