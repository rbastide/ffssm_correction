package FFSSM;

import java.time.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;



public class LicencePlongeurTest {
	// quelques objets utilitaires pour le test
	Personne bastide;
	Club biarritz;
	
	@BeforeEach
	public void setUp() {
		bastide = new Personne("1 61 03 34", "Bastide", "Rémi", null, null, null);
		biarritz = new Club(null, "Biarritz", null);
	}
	
	@Test
	public void testLicenceValide() {
		// Date de la licence
		LocalDate delivrance = LocalDate.of(2015, 1, 1); //  01/01/2015;
		// L'objet à tester
		Licence l = new Licence(bastide, "10", delivrance, biarritz);
                // La licence est valide au jour de la délivrance
 		assertTrue( l.estValide(delivrance) );
                
		// Date à tester
		// Un mois après la date de délivrance
		LocalDate unMoisApres = delivrance.plusWeeks(4);
		// La licence doit être valide un mois 
		assertTrue( l.estValide(unMoisApres) );
                
		// Date à tester
		// Un an après la date de délivrance
		LocalDate unAnApres = delivrance.plusYears(1);
		// La licence doit être valide un an 
		assertTrue( l.estValide(unAnApres) );
                
	}
	
	@Test
	public void testLicenceInvalide() {
		// Date de la licence
		LocalDate delivrance = LocalDate.of(2015, 1, 1); //  01/01/2015;
		// L'objet à tester
		Licence l = new Licence(bastide, "10", delivrance, biarritz);
		// La licence doit être invalide à cette date
		// Date à tester
		// Un an et un jour après la date de délivrance
		LocalDate d = delivrance.plusYears(1).plusDays(1);
		assertFalse( l.estValide(d) );
                // Date à tester
		// Un jour avant la date de délivrance
		LocalDate d2 = delivrance.minusDays(1);
		assertFalse( l.estValide(d2) );
	}
}
