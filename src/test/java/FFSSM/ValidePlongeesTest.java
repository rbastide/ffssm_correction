package FFSSM;


import java.time.*;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class ValidePlongeesTest {
	
	Plongeur bastide, untel;
	Club biarritz;
	Moniteur rambo;
	LocalDate datePlongee;
	
	@BeforeEach
	public void setUp() {
		biarritz = new Club(null, "Biarritz", null);

		// Quelques dates utilisées dans les tests
		datePlongee = LocalDate.of(2015, 1, 1); // 01/01/2015

		LocalDate dateBonneLicence = datePlongee.minusDays(1); // un jour avant

		LocalDate dateMauvaiseLicence = datePlongee.minusYears(1).minusDays(1); // Un an et un jour avant
		
		rambo = new Moniteur("1 61 03 34", "Rambo", "John", null, null, null, 12345);
		
		bastide = new Plongeur("1 61 03 34", "Bastide", "Rémi", null, null, null);
		Licence bonneLicence = new Licence(bastide, "123", dateBonneLicence, biarritz );
		bastide.ajouteLicence(bonneLicence);
		
		untel = new Plongeur("1 63 04 42", "Untel", "Georges", null, null, null);
		Licence mauvaiseLicence = new Licence(untel, "456", dateMauvaiseLicence, biarritz );
		untel.ajouteLicence(mauvaiseLicence);		

	}
	

	@Test
	public void testPlongeeValide() {
		Site banyuls = new Site("Banuyls sur mer", "Très joli");
		Plongee plongeeValide = new Plongee(banyuls, rambo, datePlongee, 30, 60);
		plongeeValide.ajouteParticipant(bastide);
		assertTrue(plongeeValide.estConforme());
	}
	
	@Test
	public void testPlongeeInvalide() {
		Site banyuls = new Site("Banuyls sur mer", "Très joli");
		Plongee plongeeInvalide = new Plongee(banyuls, rambo, datePlongee, 30, 60);
		plongeeInvalide.ajouteParticipant(bastide);
		plongeeInvalide.ajouteParticipant(untel);
		assertFalse(plongeeInvalide.estConforme());
	}

	@Test
	public void testPlongeesNonConformes() {
		Site banyuls = new Site("Banuyls sur mer", "Très joli");
		Plongee plongeeValide = new Plongee(banyuls, rambo, datePlongee, 30, 60);
		plongeeValide.ajouteParticipant(bastide);

		Plongee plongeeInvalide = new Plongee(banyuls, rambo, datePlongee, 30, 60);
		plongeeInvalide.ajouteParticipant(bastide);
		plongeeInvalide.ajouteParticipant(untel);
		
		biarritz.organisePlongee(plongeeValide);
		biarritz.organisePlongee(plongeeInvalide);
		
		Set<Plongee> nonConformes = biarritz.plongeesNonConformes();
		
		assertTrue(nonConformes.contains(plongeeInvalide));
		assertEquals(1, nonConformes.size());
		
	}
}
